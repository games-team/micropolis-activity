Source: micropolis-activity
Section: games
Priority: optional
Maintainer: Debian Games Team <pkg-games-devel@lists.alioth.debian.org>
Uploaders:
 Miriam Ruiz <miriam@debian.org>,
 Bruno "Fuddl" Kleinert <fuddl@debian.org>
Build-Depends:
 bison,
 debhelper-compat (= 13),
 libsdl-mixer1.2-dev,
 libx11-dev,
 libxext-dev,
 libxpm-dev,
 x11proto-xext-dev
Rules-Requires-Root: no
Standards-Version: 4.5.1
Homepage: http://www.donhopkins.com/home/micropolis/
Vcs-Git: https://salsa.debian.org/games-team/micropolis-activity.git
Vcs-Browser: https://salsa.debian.org/games-team/micropolis-activity

Package: micropolis
Architecture: any
Depends:
 micropolis-data (= ${source:Version}),
 ${misc:Depends},
 ${shlibs:Depends}
Description: real-time city management simulator
 This game simulates building and managing a whole city. The goal of the
 game is to build and design a city. The player can mark land as being
 zoned as commercial, industrial, or residential, add buildings, change
 the tax rate, build a power grid, build transportation systems and many
 other actions, in order to enhance the city.
 .
 Micropolis is the GPL-licensed version of SimCity.

Package: micropolis-data
Architecture: all
Depends: fonts-dejavu-core, ${misc:Depends}
Suggests: micropolis
Multi-Arch: foreign
Description: real-time city management simulator - data
 This game simulates building and managing a whole city. The goal of the
 game is to build and design a city. The player can mark land as being
 zoned as commercial, industrial, or residential, add buildings, change
 the tax rate, build a power grid, build transportation systems and many
 other actions, in order to enhance the city.
 .
 This package installs the data, like graphics and sounds, for the game.
